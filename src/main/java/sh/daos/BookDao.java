package sh.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import sh.entities.Book;
import sh.util.HbUtil;

public class BookDao {
	/*
	 * Create Session object From Session Factory for every new Query via method
	 * getCurrentSession().
	 */

	
	/*
	 * Using Native Query(SQL)
	 */
	public List<String> getAuthors() {
		Session session = HbUtil.getCurrentSession();
		String sql = "SELECT DISTINCT(AUTHOR) FROM books";
		NativeQuery<String> q = session.createNativeQuery(sql);
		return q.getResultList();
	}
	
	/*
	 * Using HQL
	 */
	public List<Book> findAll() {
		Session session = HbUtil.getCurrentSession();
		String hql = "from " + Book.class.getName() + " b order by b.name";
		Query<Book> q = session.createQuery(hql);
		return q.getResultList();
	}

	/*
	 * Using HQL named parameter
	 */
	public List<Book> findBySubject(String subject) {
		Session session = HbUtil.getCurrentSession();
		/*
		 * Way 1:
		String hql = String.format("from %s b where b.subject=?1", Book.class.getName());
		Query<Book> q = session.createQuery(hql);
		q.setParameter(1, subject);
		*
		*/
		
		/*
		 * Way 2:
		 */
		String hql = String.format("from %s b where b.subject=:p_subject", Book.class.getName());
		Query<Book> q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		return q.getResultList();
	}
	
	/*
	 * Using HQL
	 */
	public List<String> getSubjects() {
		Session session = HbUtil.getCurrentSession();
		String hql = "select distinct b.subject from Book b";
		Query<String> q = session.createQuery(hql);
		return q.getResultList();
	}
}