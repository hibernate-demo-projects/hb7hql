package sh.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class HbUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static ServiceRegistry registry;

	private static SessionFactory buildSessionFactory() {
		HbUtil.registry = new StandardServiceRegistryBuilder().configure().build();

		Metadata metaData = new MetadataSources(HbUtil.registry).getMetadataBuilder().build();

		return metaData.getSessionFactoryBuilder().build();
	}

	public static SessionFactory getSessionFactory() {
		return HbUtil.sessionFactory;
	}

	/*
	 * We can create session anywhere as far as same thread is running.
	 * 
	 * As our session object is stored at TLS (Thread Local Storage) we will get
	 * same session object if we call getCurrentSession() method.
	 * 
	 * If no thread is available in TLS then new thread is created and saved it in
	 * TLS. In future if getCurrentSession() is called then the same thread will be
	 * provided.
	 */
	public static Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	/*
	 * Transaction is also associated to session and as we have only one session per
	 * thread we also have only one transaction per session per thread.
	 */
	public static void beginTransaction() {
		getCurrentSession().getTransaction().begin();
	}

	public static void commitTransaction() {
		getCurrentSession().getTransaction().commit();
	}

	public static void rollbackTransaction() {
		getCurrentSession().getTransaction().rollback();
	}

	public static void shutdown() {
		HbUtil.sessionFactory.close();
	}

}
